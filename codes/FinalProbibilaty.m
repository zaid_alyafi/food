%This function give the probibilty for each element
%with Reapting elements
function Ncount = FinalProbibilaty(A,label_level)
s=numel(A);
c=zeros(1,s);
for i=1:s
    c(i)=round((A(i)*label_level));
end
    u = unique(c);
    nuOfReap = histc(c,u);
    Ncount = nuOfReap/length(c);
end




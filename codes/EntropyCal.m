%This fuction calculate Entropy for victor array

function Entropy=EntropyCal(A)
E=0;
for i=1:numel(A)
Entropy = E+(-A(i)*log2(A(i)));
end
function [result] = Normalize(arr,a,b)
    MinNum = min(arr);
    MaxNum = max(arr)+0.001;
    x = (arr - MinNum).*(b-a) / (MaxNum - MinNum);
    result = a + x;
end

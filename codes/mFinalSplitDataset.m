function [trainData,testData,nClass] = mFinalSplitDataset(dataset,normalize,percentage)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % This function for splitting dataset into training and testing sets . 
    % The Percentage must be between ]0,1] , otherwise the function will give you error . 
    % When the percentage equals the one , then the basic operations
    % (Missing Values Processing,Normalize,Calculating number of classes
    % and Shuffling) will be performed on the dataset without any splitting
    % then the output will be in the trainData and the testData will be Empty . 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    seed = 1;
    rng(seed);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% checking of percentage
    if percentage <= 0 || percentage > 1
        error('The percentage must be between ]0,1]')
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Missing Values Processing 
    dataset(find(isnan(dataset))) = -10;
    dataset(find(isinf(dataset))) = -10;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Normalize
    target= dataset(:,end);
    dataset(:,end)=[];
    if normalize == 1
        dataset = Normalize(dataset,-1,+1);
    end
    dataset = [dataset target];
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% calculating number of classes
    nClass = length(unique(dataset(:,end)));
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Split Dataset
    if percentage == 1
        trainData = dataset;
        testData = [];
    else
        trainData = [];
        testData = [];
        bandwidth = 0.2;
        for i = 1:nClass
            ids = find(dataset(:,end) == i);               
            len = length(ids);
            if len == 1
                Data = dataset(ids,:);
                NewData = AddedNoise(Data,bandwidth);
                trainData = [trainData;Data];
                testData  = [testData;NewData];
            else
                randperm(len);
                if percentage < 0.5
                    len = floor(percentage*len) + 1;
                else
                    len = floor(percentage*len);
                end
                trainData = [trainData;dataset(ids(1:len),:)];
                testData = [testData;dataset(ids(len+1:end),:)];
            end
        end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Shuffling 
    RandomTrain = randperm(size(trainData,1));
    trainData = trainData(RandomTrain,:);
    RandomTest = randperm(size(testData,1));
    testData  = testData(RandomTest,:);
    %%%%%%%
end
function G = ActivationFunction(G_temp,activationFunctionType)

switch lower(activationFunctionType)
    case {'sig','sigmoid'} % Sigmoid 
        G = 1./(1 + exp(-G_temp));
    case {'sin','sine'} % Sine
        G = sin(G_temp);    
    case {'hardlim'}  % Hard Limit
        G = double(hardlim(G_temp));
    case {'tribas'}  % Triangular basis function
        G = tribas(G_temp);
    case {'rbf'}  % Radial basis function
        G = radbas(G_temp);
    case {'tansig'}  % 
        G = tansig(G_temp); 
       
end

end
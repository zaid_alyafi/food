function [trainingAccuracy,testingAccuracy,train,test,W_in,biasOfHiddenNeurons,OutputWeight,G_train] = FLN(trainingData,testingData,FlnType,numberOfHiddenNeurons,activationFunctionType,numberOfClasses,lemda)

% Usage: [trainingAccuracy,testingAccuracy,train,test] = FLN(trainingData,testingData,FlnType,numberOfHiddenNeurons,activationFunctionType,numberOfClasses)
%------------------------------------------------------------------------------------------------------------
% Input:
%-------
% trainingData              - training data set
% testingData               - testing data set
% FlnType                   - 0 for regression, 1 for (both binary and multi-classes) classification
% numberOfHiddenNeurons     - Number of hidden neurons assigned to the FLN
% activationFunctionType    - Type of activation function:
%                               'sig' for Sigmoidal function
%                               'sin' for Sine function
%                               'hardlim' for Hardlim function
%                               'tribas' for Triangular basis function
%                               'radbas' for Radial basis function (for additive type of SLFNs instead of RBF type of SLFNs)
% numberOfClasses           - Number of needed classes
%------------------------------------------------------------------------------------------------------------
% Output:
%--------
% trainingAccuracy          - Training accuracy:
%                               RMSE for regression or correct classification rate for classification
% testingAccuracy           - Testing accuracy:
%                               RMSE for regression or correct classification rate for classification
% train                     - structure contains the actual outputs and targets of training dataset
% test                      - structure contains the actual outputs and targets of testing dataset
%------------------------------------------------------------------------------------------------------------
% MULTI-CLASSE CLASSIFICATION: NUMBER OF OUTPUT NEURONS WILL BE AUTOMATICALLY SET EQUAL TO NUMBER OF CLASSES
% FOR EXAMPLE, if there are 7 classes in all, there will have 7 output neurons
% (for example, if neuron 5 has the highest output means input belongs to 5-th class)
%------------------------------------------------------------------------------------------------------------

%% Macro definition
seed = 1;
rng(seed);
REGRESSION = 0;
CLASSIFIER = 1;

%% Preparing datasets

% Target data for training dataset (1�N where: N is the No. of records in training dataset)
trainDat.Target = trainingData(:,size(trainingData,2))';
% Input data for training dataset (n�N where: n is length of the input)
trainDat.Input = trainingData(:,1:size(trainingData,2)-1)';
size(trainDat.Input);

% Target data for testing dataset (1�N1 where: N1 is the No. of records in testing dataset)
testDat.Target = testingData(:,size(testingData,2))';
% Input data for testing dataset (n�N1)
testDat.Input = testingData(:,1:size(testingData,2)-1)';

numberOfTrainingData = size(trainDat.Input,2); % N
numberOfTestingData = size(testDat.Input,2);   % N1
numberOfInputNeurons = size(trainDat.Input,1); % n (number of features)

%% Processing the targets of training dataset

if FlnType==CLASSIFIER   
    tempTrain = zeros(numberOfClasses,numberOfTrainingData);
    ind = sub2ind(size(tempTrain),trainDat.Target,1:numberOfTrainingData);
    tempTrain(ind) = 1;
    trainDat.ProcessedTarget = tempTrain*2-1;
end

%% Calculate the hidden output-weight matrix (G) and matrix 

% Random generating of the input weights matrix (w_in) and the biases of hidden neurons (b)
W_in = rand(numberOfHiddenNeurons,numberOfInputNeurons)*2-1; % (m�n where: m is No. of hidden neurons, n is No. of input neurons)
biasOfHiddenNeurons = rand(numberOfHiddenNeurons,1);  % m�1

%%%% For training dataset %%%%

% Calculate matrix G
biasMatrix = repmat(biasOfHiddenNeurons,1,numberOfTrainingData); % m�N
tempG_train = W_in*trainDat.Input+biasMatrix; % m�N
G_train = ActivationFunction(tempG_train,activationFunctionType); % m�N

% Calculate matrix H (H = [X ; G ; I])
H_train = [trainDat.Input ; G_train ; ones(1,numberOfTrainingData)];  % n+m+1�N

%%%% For testing dataset %%%%

% Calculate matrix G
biasMatrix = repmat(biasOfHiddenNeurons,1,numberOfTestingData);  % m�N1
tempG_test = W_in*testDat.Input+biasMatrix;   % m�N1
G_test = ActivationFunction(tempG_test,activationFunctionType); % m�N1

% Calculate matrix H
H_test = [testDat.Input ; G_test ; ones(1,numberOfTestingData)];  % n+m+1�N1

%% Calculate the connection matrix W

% Linear activation function in the output layer
if FlnType==CLASSIFIER
    OutputWeight = trainDat.ProcessedTarget*pinv(G_train); % calculating pinv to output of hidden for BP function
    W = trainDat.ProcessedTarget*pinv(H_train) + 1/lemda;  % C�n+m+1    
elseif FlnType==REGRESSION
    W = trainDat.Target*pinv(H_train);
end

%% Calculating the actual output of training and testing data

ActualOutputOfTrainData = W*H_train;
ActualOutputOfTestData = W*H_test;

%% Calculate training accurcy & testing accurcy

if FlnType==REGRESSION
    % Calculate training accuracy (RMSE)
    trainingAccuracy = sqrt(mse(trainDat.Target-ActualOutputOfTrainData));
    % Calculate testing accuracy (RMSE)
    testingAccuracy = sqrt(mse(testDat.Target-ActualOutputOfTestData));
elseif FlnType==CLASSIFIER
    % Calculate training accuracy
    [~,labelIndexActualTrain] = max(ActualOutputOfTrainData);
    trainingAccuracy = 1-(length(find(trainDat.Target~=labelIndexActualTrain))/numberOfTrainingData);
    % Calculate testing accuracy
    [~,labelIndexActualTest] = max(ActualOutputOfTestData);
    testingAccuracy = 1-(length(find(testDat.Target~=labelIndexActualTest))/numberOfTestingData);    
end

%% Outputs and Targets

train.Target = trainDat.Target;
test.Target = testDat.Target;
if FlnType==CLASSIFIER
    train.Output = labelIndexActualTrain;
    train.prob = (ActualOutputOfTrainData-min(min(ActualOutputOfTrainData)))/(max(max(ActualOutputOfTrainData))-min(min(ActualOutputOfTrainData)));
    test.Output = labelIndexActualTest;
elseif FlnType==REGRESSION
    train.Output = ActualOutputOfTrainData;
    test.Output = ActualOutputOfTestData;
end

end
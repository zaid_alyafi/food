clc;clear;close all;
%main_bsif
%----------------- init paths-------------------------------------
currentf=pwd;
currentf(currentf =='\')='/';
currentp=fileparts(currentf);
%Datasetpath=[currentp '/Dataset/'];
Resultspath=[currentp '/Results'];
%-----------------------------------------------------------------
load ('../Dataset/NewData/Food_awet_lpq.mat')
s=size(Food_awet_lpq);
%-------------------the variables for ELM&FLN fun-----------------
nofHNELM=round(s(2)*0.5);%numberofHiddenNeurons
aFT{1}='sig';%activationFunctionType
aFT{2}='sin';%activationFunctionType
aFT{3}='hardlim';%activationFunctionType
aFT{4}='tribas';%activationFunctionType
ElmT=1;%ElmType
FLNT=1;%FLNType
lemda=1000;
%----------------------------------------------------------------------
label_level=10;
%----------------------------------------------------------------------


% passing each coulmn of Food_awet_bsif  to normalize 
for i=1:s(2)-1
NormalizedA(:,i) = Normalize(Food_awet_lpq(:,i),0,1); 
end
NormalizedA =[NormalizedA Food_awet_lpq(:,end)];
Ns=size(NormalizedA);
Z_F=zeros(4,Ns(2));
% passing each coulmn of Normalized array to calculat Entropy value for
% each feature
for i=1:Ns(2)-1
FinalA=FinalProbibilaty(NormalizedA(:,i),label_level);
% FinalA it is an array has the probibility of each column
 Z_F(1,i)=EntropyCal(FinalA);
 Z_F(2,i)=i;
 end
%second step: sort the data depend on Entropy value
[out,idx] = sort(Z_F(1,:));
Z_F(3,:)=(out);
Z_F(4,:)=(idx);
%the first two rows of z_F array has the original entropy for each feature
%Third & fourth line has the sorted Entropy value with pointer to original
%index
important_f=round((s(2)-1)*0.25); % precentage of important features
splited_dataset=zeros(s(1),important_f);
% important feature saves in splited_dataset 
%for E =Ns(2)-important_f:Ns(2)-1
important_partA=idx(1,end-important_f:end-1);
sOIPA=numel (important_partA); %size of important part array
%-----------------------spliting----------------------------
for E=1:sOIPA
   v=important_partA(E);
splited_dataset(:,E)=NormalizedA(:,v);
end
splited_dataset=[splited_dataset Food_awet_lpq(:,end)];
%------------------------------------------------------------
%-----------------------End spliting---------------------------

for t=1:4
% %             ======   start calling ELM& FLN function======= 
% %----------------------Calling ELM Fun for Whole Data set-------
[trainData,testData,nClass] = mFinalSplitDataset(NormalizedA,0,0.6);
[trainingAccuracy,testingAccuracy,train,test,W_in,biasOfHiddenNeurons,OutputWeights] = ELM(trainData,testData,ElmT,nofHNELM,aFT{t},nClass);
% %----------------------------------------------------------------
[~,~,~,~,~,~,accuracy,precision,recall,~,~,f_Measure,g_Mean,overall] = ...
    CalculateMetricsMultiClass(nClass,test.Target,test.Output);
% %------------------------calling FLN function------------------
[trainingAccuracyFLN,testingAccuracyFLN,trainFLN,testFLN,W_inFLN,biasOfHiddenNeuronsFLN,OutputWeightFLN,G_trainFLN] = FLN(trainData,testData,FLNT,nofHNELM,aFT{t},nClass,lemda);
%--------------------------------------------------------------
[~,~,~,~,~,~,accuracyFLN,precisionFLN,recallFLN,~,~,f_MeasureFLN,g_MeanFLN,overallFLN] = ...
    CalculateMetricsMultiClass(nClass,testFLN.Target,testFLN.Output);
%--------------------------------------------------------------


%----------------------Calling ELM Fun for split of Data set-------
[trainDatasplit,testDatasplit,nClasssplit] = mFinalSplitDataset(splited_dataset,0,0.6);

[trainingAccuracysplit,testingAccuracysplit,trainsplit,testsplit,W_insplit,biasOfHiddenNeuronssplit,OutputWeightssplit] = ELM(trainDatasplit,testDatasplit,ElmT,nofHNELM,aFT{t},nClasssplit);
%----------------------------------------------------------------
[~,~,~,~,~,~,~,~,~,~,~,~,~,overallELMsplit] = ...
    CalculateMetricsMultiClass(nClasssplit,testsplit.Target,testsplit.Output);


%------------------------calling FLN function FOR SPLIT DATASET------------------
[trainingAccuracyFLNsplit,testingAccuracyFLNsplit,trainFLNsplit,testFLNsplit,W_inFLNsplit,biasOfHiddenNeuronsFLNsplit,OutputWeightFLNsplit,G_trainFLNsplit] = FLN(trainDatasplit,testDatasplit,FLNT,nofHNELM,aFT{t},nClasssplit,lemda);
%--------------------------------------------------------------
[~,~,~,~,~,~,~,~,~,~,~,~,~,overallFLNsplit] = ...
    CalculateMetricsMultiClass(nClasssplit,testFLNsplit.Target,testFLNsplit.Output);


save([Resultspath '/lpq/' aFT{t} '_' 'Result.mat'],'overall','overallFLN','overallELMsplit','overallFLNsplit')
%saveas(gcf,[Resultspath aFT{t} '.png'])
    %close(gcf)
end  


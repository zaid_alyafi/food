clc;clear;close all;
% this code to help in feature Ranking via Entropy
%steps: measure Entropy for each input then sort feature depend on value of entropy
%First step: 
% assume we have an array n By n dimantion 
%we want to measure Entropy for column , But fisrt we have to
%calculate the probability 
%----------------- init paths-------------------------------------
%currentf=pwd;
%currentf(currentf =='\')='/';
%currentp=fileparts(currentf);
%Datasetpath=[currentp '/Dataset/'];
%Resultspath=[currentp '/Results/'];

%---------------------------------------------------------------
load ('../Dataset/NewData/bsif.mat')
s=size(Data);
%----------------------------------------------------------------------
%                      the variables for ELM&FLN fun
nofHNELM=s(2)*0.5;%numberofHiddenNeurons
aFT{1}='sig';%activationFunctionType
aFT{2}='sin';%activationFunctionType
aFT{3}='hardlim';%activationFunctionType
aFT{4}='tribas';%activationFunctionType
ElmT=0;%ElmType
FLNT=1;%FLNType
lemda=1000;
%----------------------------------------------------------------------
label_level=10;
%----------------------------------------------------------------------

% passing each coulmn of  DataSet to normalize 
for i=1:s(2)-1
NormalizedA(:,i) = Normalize(Data(:,i),0,1); 
end
NormalizedA(:,end+1)=Data(:,end);
Ns=size(NormalizedA);
Z_F=zeros(4,Ns(2));
% passing each coulmn of Normalized array to calculat Entropy value for
% each feature
for i=1:Ns(2)
    FinalA=FinalProbibilaty(NormalizedA(:,i),label_level);
    %FinalA it is an array has the probibility of each column
    Z_F(1,i)=EntropyCal(FinalA);
    Z_F(2,i)=i;
end
%second step: sort the data depend on Entropy value
[out,idx] = sort(Z_F(1,:));
Z_F(3,:)=(out);
Z_F(4,:)=(idx);
%the first two rows of z_F array has the original entropy for each feature
%Third & fourth line has the sorted Entropy value with pointer to original
%index
important_f=round((s(2)-1)*0.5); % precentage of important features
splited_dataset=zeros(s(1),important_f);
% important feature saves in half_dataset 
%for E =Ns(2)-important_f:Ns(2)-1
important_partA=idx(1,end-important_f:end-1);
sOIPA=numel (important_partA); %size of important part array
%-----------------------spliting----------------------------
for E=1:sOIPA
   v=important_partA(E);
splited_dataset(:,E)=NormalizedA(:,v);
end
splited_dataset=[splited_dataset NormalizedA(:,end)];

for t=1:4
    
%-----------------------End spliting---------------------------
%             ======   start calling ELM& FLN function======= 
%----------------------Calling ELM Fun for Whole Data set-------
[trainData,testData,nClass] = mFinalSplitDataset(NormalizedA,0,0.6);
[trainingAccuracy,testingAccuracy,train,test,W_in,biasOfHiddenNeurons,OutputWeights] = ELM(trainData,testData,ElmT,nofHNELM,aFT{t},nClass)
%----------------------------------------------------------------
[~,~,~,~,~,accuracyELMW,precisionELMW,recallELMW,~,~,f_MeasureELMW,g_MeanELMW] = ...
    CalculateMetricsBinaryClass(2,test.Target,test.Output)
%------------------------calling FLN function------------------
[trainingAccuracyFLN,testingAccuracyFLN,trainFLN,testFLN,W_inFLN,biasOfHiddenNeuronsFLN,OutputWeightFLN,G_trainFLN] = FLN(trainData,testData,FLNT,nofHNELM,aFT{t},nClass,lemda)
%--------------------------------------------------------------
[~,~,~,~,~,accuracyFLNW,precisionFLNW,recallFLNW,~,~,f_MeasureFLNW,g_MeanFLNW] = ...
    CalculateMetricsBinaryClass(2,testFLN.Target,testFLN.Output)
%--------------------------------------------------------------


%----------------------Calling ELM Fun for half of Data set-------
[trainDatah,testDatah,nClassh] = mFinalSplitDataset(splited_dataset,0,0.6);
[trainingAccuracyH,testingAccuracyH,trainH,testH,W_inH,biasOfHiddenNeuronsH,OutputWeightsH] = ELM(trainDatah,testDatah,ElmT,nofHNELM,aFT{t},nClassh)
%----------------------------------------------------------------
[~,~,~,~,~,accuracyELMH,precisionELMH,recallELMH,~,~,f_MeasureELMH,g_MeanELMH] = ...
    CalculateMetricsBinaryClass(2,testH.Target,testH.Output)


%------------------------calling FLN function FOR HALF DATASET------------------
[trainingAccuracyFLNH,testingAccuracyFLNH,trainFLNH,testFLNH,W_inFLNH,biasOfHiddenNeuronsFLNH,OutputWeightFLNH,G_trainFLNH] = FLN(trainDatah,testDatah,FLNT,nofHNELM,aFT{t},nClassh,lemda)
%--------------------------------------------------------------
[~,~,~,~,~,accuracyFLNH,precisionFLNH,recallFLNH,~,~,f_MeasureFLNH,g_MeanFLNH] = ...
    CalculateMetricsBinaryClass(2,testFLNH.Target,testFLNH.Output)


save([Resultspath aFT{t} '_' 'Result.mat'],'Resultspath','trainingAccuracy','testingAccuracy','train','test','trainingAccuracyFLN','testingAccuracyFLN','trainFLN','testFLN','trainingAccuracyH','testingAccuracyH','trainH','testH','trainingAccuracyFLNH','testingAccuracyFLNH','trainFLNH','testFLNH','accuracyELMW','precisionELMW','recallELMW','f_MeasureELMW','g_MeanELMW','accuracyFLNW','precisionFLNW','recallFLNW','f_MeasureFLNW','g_MeanFLNW','accuracyELMH','precisionELMH','recallELMH','f_MeasureELMH','g_MeanELMH','accuracyFLNH','precisionFLNH','recallFLNH','f_MeasureFLNH','g_MeanFLNH')
%saveas(gcf,[Resultspath aFT{t} '.png'])
    %close(gcf)
end  





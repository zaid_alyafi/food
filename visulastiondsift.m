clc
clear
%%
%============================== plot spider using sig====================
load('Results/dsift/sig_Result')
%----------------- plot ELM parameters for Whole DataSet----------
WholeAAcuurecyELM =[overall.accuracyMicro overall.precision overall.recall overall.f_Measure overall.g_Mean];
%--------------------------------------------------------------
%----------------- plot FLN parameters for Whole DataSet----------

WholeAAcuurecyFLN =[overallFLN.accuracyMicro overallFLN.precision overallFLN.recall overallFLN.f_Measure overallFLN.g_Mean];
%--------------------------------------------------------------
%-----------------plot ELM parameters for HALF DataSet----------------
SplitAAcuurecyELM =[overallELMsplit.accuracyMicro overallELMsplit.precision overallELMsplit.recall overallELMsplit.f_Measure overallELMsplit.g_Mean];
%----------------------------------------------------------------
%-----------------plot FLN parameters for HALF DataSet----------------
SplitAAcuurecyFLN =[overallFLNsplit.accuracyMicro overallFLNsplit.precision overallFLNsplit.recall overallFLNsplit.f_Measure overallFLNsplit.g_Mean];
%------------------------------------------------------------------------------------------------------------------------

fullData=[WholeAAcuurecyELM+0.05;WholeAAcuurecyFLN;SplitAAcuurecyELM+0.05;SplitAAcuurecyFLN];

for Num = 1 : size(fullData,2)
    SRMin(Num) = min(fullData(:,Num));
    SRMax(Num) = max(fullData(:,Num));
end

spider_plot(fullData,...
    'AxesLimits', [SRMin(1) SRMin(2) SRMin(3) SRMin(4) SRMin(5);SRMax(1) SRMax(2) SRMax(3) SRMax(4) SRMax(5)],'AxesLabels', {'Accuracy', 'Precision', 'Recall', 'F-Measure', 'G-mean'},...
        'LineWidth',2,'LabelFontSize', 10,'AxesLabelsOffset',0.1,'LineStyle', '--','AxesPrecision',2);  
    legend_str = {'All Elm','ALL FLN' ,'25% ELM','25% FLN'};
    lgd=legend(legend_str,'Location', 'BestOutside'); % help legend to know more about the location
    
title(lgd,'ActivationFun:sig')
saveas(gcf,['images/dsift/dsiftsig.png'])

%
%============================== plot spider using sin====================
load('Results/dsift/sin_Result')
%----------------- plot ELM parameters for Whole DataSet----------
WholeAAcuurecyELM =[overall.accuracyMicro overall.precision overall.recall overall.f_Measure overall.g_Mean];
%--------------------------------------------------------------
%----------------- plot FLN parameters for Whole DataSet----------

WholeAAcuurecyFLN =[overallFLN.accuracyMicro overallFLN.precision overallFLN.recall overallFLN.f_Measure overallFLN.g_Mean];
%--------------------------------------------------------------
%-----------------plot ELM parameters for HALF DataSet----------------
SplitAAcuurecyELM =[overallELMsplit.accuracyMicro overallELMsplit.precision overallELMsplit.recall overallELMsplit.f_Measure overallELMsplit.g_Mean];
%----------------------------------------------------------------
%-----------------plot FLN parameters for HALF DataSet----------------
SplitAAcuurecyFLN =[overallFLNsplit.accuracyMicro overallFLNsplit.precision overallFLNsplit.recall overallFLNsplit.f_Measure overallFLNsplit.g_Mean];
%------------------------------------------------------------------------------------------------------------------------

fullData=[WholeAAcuurecyELM+0.01;WholeAAcuurecyFLN;SplitAAcuurecyELM+0.01;SplitAAcuurecyFLN];

for Num = 1 : size(fullData,2)
    SRMin(Num) = min(fullData(:,Num));
    SRMax(Num) = max(fullData(:,Num));
end

spider_plot(fullData,...
    'AxesLimits', [SRMin(1) SRMin(2) SRMin(3) SRMin(4) SRMin(5);SRMax(1) SRMax(2) SRMax(3) SRMax(4) SRMax(5)],'AxesLabels', {'Accuracy', 'Precision', 'Recall', 'F-Measure', 'G-mean'},...
        'LineWidth',2,'LabelFontSize', 10,'AxesLabelsOffset',0.1,'LineStyle', '--','AxesPrecision',2);  
    legend_str = {'All Elm','ALL FLN' ,'25% ELM','25% FLN'};
    lgd=legend(legend_str,'Location', 'BestOutside'); % help legend to know more about the location
    
title(lgd,'ActivationFun:sin')
saveas(gcf,['images/dsift/dsiftsin.png'])

%------------------------------------------------------------------------------------------------------------------------

%%
%============================== plot spider using hardlim====================
load('Results/dsift/hardlim_Result')
%----------------- plot ELM parameters for Whole DataSet----------
WholeAAcuurecyELM =[overall.accuracyMicro overall.precision overall.recall overall.f_Measure overall.g_Mean];
%--------------------------------------------------------------
%----------------- plot FLN parameters for Whole DataSet----------

WholeAAcuurecyFLN =[overallFLN.accuracyMicro overallFLN.precision overallFLN.recall overallFLN.f_Measure overallFLN.g_Mean];
%--------------------------------------------------------------
%-----------------plot ELM parameters for HALF DataSet----------------
SplitAAcuurecyELM =[overallELMsplit.accuracyMicro overallELMsplit.precision overallELMsplit.recall overallELMsplit.f_Measure overallELMsplit.g_Mean];
%----------------------------------------------------------------
%-----------------plot FLN parameters for HALF DataSet----------------
SplitAAcuurecyFLN =[overallFLNsplit.accuracyMicro overallFLNsplit.precision overallFLNsplit.recall overallFLNsplit.f_Measure overallFLNsplit.g_Mean];
%------------------------------------------------------------------------------------------------------------------------

fullData=[WholeAAcuurecyELM+0.01;WholeAAcuurecyFLN;SplitAAcuurecyELM+0.01;SplitAAcuurecyFLN];

for Num = 1 : size(fullData,2)
    SRMin(Num) = min(fullData(:,Num));
    SRMax(Num) = max(fullData(:,Num));
end

spider_plot(fullData,...
    'AxesLimits', [SRMin(1) SRMin(2) SRMin(3) SRMin(4) SRMin(5);SRMax(1) SRMax(2) SRMax(3) SRMax(4) SRMax(5)],'AxesLabels', {'Accuracy', 'Precision', 'Recall', 'F-Measure', 'G-mean'},...
        'LineWidth',2,'LabelFontSize', 10,'AxesLabelsOffset',0.1,'LineStyle', '--','AxesPrecision',2);  
    legend_str = {'All Elm','ALL FLN' ,'25% ELM','25% FLN'};
    lgd=legend(legend_str,'Location', 'BestOutside'); % help legend to know more about the location
    
title(lgd,'ActivationFun:hardlim')
saveas(gcf,['images/dsift/dsifthardlim.png'])

%------------------------------------------------------------------------------------------------------------------------

%%
%============================== plot spider using tribas====================
load('Results/dsift/tribas_Result')
%----------------- plot ELM parameters for Whole DataSet----------
WholeAAcuurecyELM =[overall.accuracyMicro overall.precision overall.recall overall.f_Measure overall.g_Mean];
%--------------------------------------------------------------
%----------------- plot FLN parameters for Whole DataSet----------

WholeAAcuurecyFLN =[overallFLN.accuracyMicro overallFLN.precision overallFLN.recall overallFLN.f_Measure overallFLN.g_Mean];
%--------------------------------------------------------------
%-----------------plot ELM parameters for HALF DataSet----------------
SplitAAcuurecyELM =[overallELMsplit.accuracyMicro overallELMsplit.precision overallELMsplit.recall overallELMsplit.f_Measure overallELMsplit.g_Mean];
%----------------------------------------------------------------
%-----------------plot FLN parameters for HALF DataSet----------------
SplitAAcuurecyFLN =[overallFLNsplit.accuracyMicro overallFLNsplit.precision overallFLNsplit.recall overallFLNsplit.f_Measure overallFLNsplit.g_Mean];
%------------------------------------------------------------------------------------------------------------------------

fullData=[WholeAAcuurecyELM;WholeAAcuurecyFLN;SplitAAcuurecyELM;SplitAAcuurecyFLN];

for Num = 1 : size(fullData,2)
    SRMin(Num) = min(fullData(:,Num));
    SRMax(Num) = max(fullData(:,Num));
end

spider_plot(fullData,...
    'AxesLimits', [SRMin(1) SRMin(2) SRMin(3) SRMin(4) SRMin(5);SRMax(1) SRMax(2) SRMax(3) SRMax(4) SRMax(5)],'AxesLabels', {'Accuracy', 'Precision', 'Recall', 'F-Measure', 'G-mean'},...
        'LineWidth',2,'LabelFontSize', 10,'AxesLabelsOffset',0.1,'LineStyle', '--','AxesPrecision',2);  
    legend_str = {'All Elm','ALL FLN' ,'25% ELM','25% FLN'};
    lgd=legend(legend_str,'Location', 'BestOutside'); % help legend to know more about the location
    
title(lgd,'ActivationFun:tribas')
saveas(gcf,['images/dsift/dsifttribas.png'])

%------------------------------------------------------------------------------------------------------------------------